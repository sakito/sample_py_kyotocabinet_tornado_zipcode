all: ken_all start

ken_all:
	curl -O http://www.post.japanpost.jp/zipcode/dl/oogaki/zip/ken_all.zip
	unzip -d data ken_all.zip
	python data_load.py

setup:
	sudo port install kyotocabinet +lzma +lzo
	pip install -U http://fallabs.com/kyotocabinet/pythonlegacypkg/kyotocabinet-python-legacy-1.16.tar.gz
	pip install -U tornado

start:
	python server.py